# My responses

* The tests can be executed from either of the following:

1. Terminal using "mvn test" (from project directory)
2. Running src/test/resources/testng.xml (context menu -> Run) in IDE
3. By clicking on play button next to tests/classes in IDE

* Why this approach:

1. RestAssured allows easy rest api handling
2. Java - because I have most experience with it

* To improve:

1. I would use a docker image of the service and run the tests against it. The dependecies would be covered either by docker images if available or would be mocked
2. I would write rainy tests if there was specification available for it.
3. I would report issues in a bug tracker and ask for clarifications, e.g. there is an extra field in draw cards response - "images" per each card which wasn't defined on the website, is it by interntional?
4. I would test each api separately instead of long scenarios (perhaps using db manipulation)
