package config;

public interface Constants {

    interface Endpoints {
        String API_URL = "http://deckofcardsapi.com/api/deck/%s/";
        String SHUFFLED_URL = API_URL + "shuffle/?deck_count=1";
        String DRAW_URL = API_URL + "draw/?count=%s";
        String RETURN_URL = API_URL + "return/?cards=%s";
    }

}
