package helpers.dto;

import java.util.Objects;

public class Piles {

    private Discard discard;

    public Discard getDiscard() {
        return discard;
    }

    public void setDiscard(Discard discard) {
        this.discard = discard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Piles)) return false;
        Piles piles = (Piles) o;
        return Objects.equals(discard, piles.discard);
    }

    @Override
    public int hashCode() {

        return Objects.hash(discard);
    }

    @Override
    public String toString() {
        return "Piles{" +
                "discard=" + discard +
                '}';
    }
}
