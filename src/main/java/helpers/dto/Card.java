package helpers.dto;


import java.util.HashMap;
import java.util.Objects;

public class Card {

    private String image;
    private String value;
    private String suit;
    private String code;
    private HashMap images;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSuit() {
        return suit;
    }

    public void setSuit(String suit) {
        this.suit = suit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public HashMap getImages() {
        return images;
    }

    public void setImages(HashMap images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;
        Card card = (Card) o;
        return Objects.equals(image, card.image) &&
                Objects.equals(value, card.value) &&
                Objects.equals(suit, card.suit) &&
                Objects.equals(code, card.code) &&
                Objects.equals(images, card.images);
    }

    @Override
    public int hashCode() {

        return Objects.hash(image, value, suit, code, images);
    }

    @Override
    public String toString() {
        return "Card{" +
                "image='" + image + '\'' +
                ", value='" + value + '\'' +
                ", suit='" + suit + '\'' +
                ", code='" + code + '\'' +
                ", images=" + images +
                '}';
    }
}
