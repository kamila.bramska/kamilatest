package helpers.dto;

import java.util.Objects;

public class Discard {

    private Integer remaining;

    public Integer getRemaining() {
        return remaining;
    }

    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Discard)) return false;
        Discard discard = (Discard) o;
        return Objects.equals(remaining, discard.remaining);
    }

    @Override
    public int hashCode() {

        return Objects.hash(remaining);
    }

    @Override
    public String toString() {
        return "Discard{" +
                "remaining=" + remaining +
                '}';
    }
}
