package helpers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class ReturnResponse {

    private Boolean success;
    @JsonProperty("deck_id")
    private String deckId;
    private Boolean shuffled;
    private Integer remaining;
    private Piles piles;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public Boolean getShuffled() {
        return shuffled;
    }

    public void setShuffled(Boolean shuffled) {
        this.shuffled = shuffled;
    }

    public Integer getRemaining() {
        return remaining;
    }

    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    public Piles getPiles() {
        return piles;
    }

    public void setPiles(Piles piles) {
        this.piles = piles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReturnResponse)) return false;
        ReturnResponse that = (ReturnResponse) o;
        return Objects.equals(success, that.success) &&
                Objects.equals(deckId, that.deckId) &&
                Objects.equals(shuffled, that.shuffled) &&
                Objects.equals(remaining, that.remaining) &&
                Objects.equals(piles, that.piles);
    }

    @Override
    public int hashCode() {

        return Objects.hash(success, deckId, shuffled, remaining, piles);
    }

    @Override
    public String toString() {
        return "ReturnResponse{" +
                "success=" + success +
                ", deckId='" + deckId + '\'' +
                ", shuffled=" + shuffled +
                ", remaining=" + remaining +
                ", piles=" + piles +
                '}';
    }
}
