package helpers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class CardsResponse {

    private Boolean success;
    @JsonProperty("deck_id")
    private String deckId;
    private List<Card> cards;
    private Integer remaining;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public Integer getRemaining() {
        return remaining;
    }

    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CardsResponse)) return false;
        CardsResponse that = (CardsResponse) o;
        return Objects.equals(success, that.success) &&
                Objects.equals(deckId, that.deckId) &&
                Objects.equals(cards, that.cards) &&
                Objects.equals(remaining, that.remaining);
    }

    @Override
    public int hashCode() {

        return Objects.hash(success, deckId, cards, remaining);
    }

    @Override
    public String toString() {
        return "CardsResponse{" +
                "success=" + success +
                ", deckId='" + deckId + '\'' +
                ", cards=" + cards +
                ", remaining=" + remaining +
                '}';
    }
}
