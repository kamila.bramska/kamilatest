package helpers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class NewDeckResponse {

    private Boolean success;
    @JsonProperty("deck_id")
    private String deckId;
    private Boolean shuffled;
    private Integer remaining;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public Boolean getShuffled() {
        return shuffled;
    }

    public void setShuffled(Boolean shuffled) {
        this.shuffled = shuffled;
    }

    public Integer getRemaining() {
        return remaining;
    }

    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewDeckResponse)) return false;
        NewDeckResponse that = (NewDeckResponse) o;
        return Objects.equals(success, that.success) &&
                Objects.equals(deckId, that.deckId) &&
                Objects.equals(shuffled, that.shuffled) &&
                Objects.equals(remaining, that.remaining);
    }

    @Override
    public int hashCode() {

        return Objects.hash(success, deckId, shuffled, remaining);
    }

    @Override
    public String toString() {
        return "NewDeckResponse{" +
                "success=" + success +
                ", deckId='" + deckId + '\'' +
                ", shuffled=" + shuffled +
                ", remaining=" + remaining +
                '}';
    }
}
