package helpers.util;

import helpers.dto.CardsResponse;
import helpers.dto.NewDeckResponse;
import helpers.dto.ReturnResponse;
import io.restassured.response.Response;
import org.testng.Assert;

public class Responses {

    public static String validateNewDeckResponse(Response response)
    {
        response.then();
//                .log().all();
        NewDeckResponse responseBody = response.then().statusCode(200).extract().as(NewDeckResponse.class);
        Assert.assertTrue(responseBody.getSuccess());
        Assert.assertTrue(responseBody.getRemaining() == 52);
        Assert.assertTrue(responseBody.getShuffled());
        Assert.assertNotNull(responseBody.getDeckId());

        return responseBody.getDeckId();
    }

    public static CardsResponse validateCardsResponse(Response response, Integer previousRemaining, Integer cardNumber, String deckId)
    {
        response.then();
//                .log().all();
        CardsResponse responseBody = response.then().statusCode(200).extract().as(CardsResponse.class);
        Assert.assertTrue(responseBody.getSuccess());
        Assert.assertTrue(responseBody.getRemaining() == (previousRemaining - cardNumber));
        Assert.assertEquals(responseBody.getDeckId(), deckId);
        Assert.assertNotNull(responseBody.getCards());
        Assert.assertTrue(responseBody.getCards().size() == cardNumber);

        return responseBody;
    }

    public static Integer validateReturnResponse(Response response, Integer cardNumber, String deckId)
    {
        response.then();
//                .log().all();
        ReturnResponse responseBody = response.then().statusCode(200).extract().as(ReturnResponse.class);
        Assert.assertTrue(responseBody.getSuccess());
        Assert.assertTrue(responseBody.getRemaining() == (53 - cardNumber));
        Assert.assertEquals(responseBody.getDeckId(), deckId);

        return responseBody.getRemaining();
    }

}
