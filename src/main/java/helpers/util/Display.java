package helpers.util;

public class Display {

    public static void displaySuccess(Thread thread) {
        System.out.println("\u001b[32m" + String.format("        %s.%s PASSED\u001b[0m", thread.getStackTrace()[2].getClassName(), thread.getStackTrace()[2].getMethodName()));
        System.out.println();
    }
}
