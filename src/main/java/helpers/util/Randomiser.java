package helpers.util;

public class Randomiser {

    public static Integer getRandomInteger() {
        Integer randomised = 1 + (int)(Math.random() * ((5 - 1) + 1));
        return randomised;
    }
}
