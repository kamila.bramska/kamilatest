package helpers.util;

import config.Constants;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.HashMap;

public class Requests {

    public static Response postRequest(String url)
    {
        return RestAssured.given()
//                .log().all()
                .contentType(ContentType.JSON)
                .post(url);
    }

    public static Response getRequest(String url)
    {
        return RestAssured.given()
//                .log().all()
                .contentType(ContentType.JSON)
                .get(url);
    }
}
