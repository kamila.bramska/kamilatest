package sunny;

import helpers.dto.CardsResponse;
import helpers.util.Display;
import helpers.util.Randomiser;
import helpers.util.Requests;
import helpers.util.Responses;
import org.testng.annotations.Test;

import static config.Constants.Endpoints.DRAW_URL;
import static config.Constants.Endpoints.RETURN_URL;
import static config.Constants.Endpoints.SHUFFLED_URL;

public class Scenario1 {

    @Test
    public void shuffleNewDeckDrawCardsReturnAndCheckRemaining () {

        String deckId = Responses.validateNewDeckResponse(Requests.postRequest(String.format(SHUFFLED_URL, "new")));
        System.out.println("New deck created and shuffled - deck_id = " + deckId);

        Integer cardNumber;
        Integer totalCardNumber = 0;
        Integer cardsRemaining = 52;
        String firstCardCode = "";
        CardsResponse cardsResponse;
        for (int i = 0; i < 5; i++) {
            cardNumber = Randomiser.getRandomInteger();
            System.out.println("Drawing " + cardNumber + " cards");
            cardsResponse = Responses.validateCardsResponse(Requests.getRequest(String.format(DRAW_URL, deckId, cardNumber)), cardsRemaining, cardNumber, deckId);
            totalCardNumber = totalCardNumber + cardNumber;
            cardsRemaining = cardsResponse.getRemaining();
            if (i == 0) {
                firstCardCode = cardsResponse.getCards().get(0).getCode();
                System.out.println("Card to return is " + firstCardCode);
            }
        }

        Integer cardsLeft = Responses.validateReturnResponse(Requests.getRequest(String.format(RETURN_URL, deckId, firstCardCode)), totalCardNumber, deckId);
        System.out.println("The first card successfully returned to the deck");
        System.out.println("Number of cards left in the deck is " + cardsLeft);

        Display.displaySuccess(Thread.currentThread());
    }
}
